## **Test de compétences admin sys (junior)**

### **Questions de bon sens**
1. Pour réaliser des modifications sur un service en production, il faut d'abord éliminer les risques d'interruption, pour ce faire il faut suivre la procédure ci-dessous:
- Après avoir cloné le projet, il est recommendé d'installer un workflow pour gérer les branches du projet.
- Il faut créer une branche de test en local, et y attribuer les différentes modifications souhaitables.
- On commit ensuite cette branche sur le dépôt distant pour qu'elle soit visualisée par le reste des membres des équipes et testée.
- Lorsque le teste est validé, on passe à la ' Merge request'
- A cette étape, la branche doit être approouvée par les administrateurs de l'équipes pour pouvoir la déployer en recette, puis en pré-production et finalement en production.
- De cette manière, le risque de toucher à la disponibilité ou de rupture de service est minimal.
- D'autre part le support doit être permanent pour minimiser le temps d'une éventuelle rupture, et pouvoir corriger les défaillances plus rapidement.

2. Il y a deux façons de faire les choses avec des autorisations élevées.
sudo permet d'exécuter des commandes dans votre propre compte utilisateur avec les privilèges de root.
su vous permet de changer d'utilisateur de façon à être réellement connecté en tant que root. Mais cette option est désactivée par défaut sur Ubuntu. À la place, vous pouvez simplement lancer un shell root avec sudo -i.
N'utilisez l'une ou l'autre de ces méthodes qu'au fur et à mesure des besoins, car elles peuvent endommager votre système si elles sont utilisées sans précaution.

3. Les fichiers temporaires sont généralement créés dans le répertoire temporaire (tel que /tmp) où tous les autres utilisateurs et processus ont un accès en lecture et en écriture (tout autre script peut y créer les nouveaux fichiers). Le script doit donc faire attention à la création des fichiers, par exemple en les utilisant avec les bonnes autorisations (par exemple, lecture seule pour le propriétaire, voir : aide umask) et le nom du fichier ne doit pas être facile à deviner (idéalement aléatoire). Sinon, si les noms de fichiers ne sont pas uniques, cela peut créer un conflit avec le même script exécuté plusieurs fois (par exemple, une condition de course) ou un attaquant pourrait soit détourner certaines informations sensibles (par exemple, lorsque les autorisations sont trop ouvertes et que le nom de fichier est facile à deviner), soit créer/remplacer le fichier avec sa propre version du code (par exemple, remplacer les commandes ou les requêtes SQL en fonction de ce qui est stocké).



### **Questions de culture générale**

1. Parmi les caractéristiques de la philosophie unix, on retrouve : le système de fichiers hiérarchisés en arbre, le système de processus et le terminal de controle

2. Le protocole SNMP permet d'administrer les équipements réseau par exemple les switchs, les hubs, les routeurs ou les serveurs, et assurer une meilleure communication entre les superviseurs et les agents.

3. La principale mission du Haproxy est de réaliser la répartition de charge ( Load-balancing) entre les serveurs web 

4. On peut utiliser une clé SSH pour se connecter à une machine distante et l'administrer sans l'utiliser en direct, ou par exemple pour pouvoir cloner des projets depuis git et connecter sa machine locale au dépot distant de Git.


### **Questions d'administration Unix**
Voir le fichier test1.sh 

### **Questions de scripting Shell**

2. La façon normale que les gens utilisent cron est via la commande crontab. Cela vous permet de voir ou éditer votre fichier crontab, qui est un fichier par-utilisateur contenant des entrées décrivant les commandes pour exécuter et le temps pour les exécuter.

4.  Voir test1.sh


### **Questions d'administration réseau**

1.  l'adresse réseau est: 10.40.0.0/19 , l'adresse de broadcast est : 10.40.255.255 et le nombre de machines possible est 2^6=32
Vous disposez du réseau 10.42.0.0/16. Vous devez fournir 100 sous-réseaux pouvant contenir au
moins 220 machines. Proposez un découpage, et expliquez le.
2. 10.42.0.0
   10.42.0.00000001
   .
   .
   .
   10.42.255.255


### **Questions avancées 

1. En informatique, un appel système  désigne le moment où un programme s'interrompt pour demander au système d'exploitation d'accomplir pour lui une certaine tâche. L'expression désigne donc aussi la fonction primitive elle-même fournie par le noyau d'un système d'exploitation. Sont ainsi contrôlées et uniformisées les applications de l'espace utilisateur ; certains droits d'accès peuvent de plus être réservés au noyau. Le noyau lui-même utilise ces fonctions, qui deviennent la seule porte entre un programme et le « monde extérieur ».

2. Strace est un utilitaire permettant de tracer/suivre les appels systèmes.
Les appels systèmes sont les interfaces fondamentales entre les applications et le noyau.
Généralement, ils ne sont pas appelés directement, mais via des wrappers de la glibc.
En utilisant strace, on peut intercepter ces appels systèmes pour un processus ou une commande donnée.
strace est donc un outil puissant de dépannage pour tous les administrateurs et utilisateurs unix/linux.

3. Le disk libre : df -h
Espace disk utilisé : du -a
État de Ram : htop

4. Ps -aux | grep /tmp


## **Test de compétences Git**

### **Utilisation de Git**
1. Il faut suivre les étapes ci-après:
- Cloner le repository en local : git pull git://git.example.com/foo
- Rentrer dans le repo en local cd foo
- Créer une nouvelle branche à partir de master : git checkout -b feat_foo origin/master
- Ouvrir le fichier foo.py et faire les modifications demandées
- Réaliser des test en local si c'est possible
- Pour vérifier les modifications réalisée : git diff
- Si on a besoin d'ajouter d'autres fichiers : git add .
- Commiter les changement sur notre branche : git commit -a -m 'Update foo.py file'
- Pusher les changement sur la branche : git push origin feat_foo
- Créer une pull request (PR) de ma branche vers master et l'assigner à un mebre de l'équipe pour la validation
- Une fois validée : merger la branche dans master et supprimer la branche si nécessaire

2. Si la modification est poussé dans master, on peut créer une branche v1.0 à partir de master
git branche v1.0 

3. Une fois le développement est validé sur newfeature, on crée une Merge request de newfeature vers master, 
pour faire aller sur le repo (via le navigateur), Merge requests, create new merge request, spécifier les branches, 
Après valider, et Squach and merge
