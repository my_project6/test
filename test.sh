##############
# Question 1 #
##############

find -name ".py" -R

##############
# Question 2 #
##############

find -name "*.pyc" -delete -R

##############
# Question 3 #
##############

head -n+100 fichier.txt| tail -n+21 

##############
# Question 4 #
##############

sudo chown user1 /data/mixte
sudo chown user2 /data/mixte
sudo chmod g+rwx /data/mixte

##############
# Question 5 #
##############

redirect test.example.com http://dev.example.com 

##################
# script part 4  #
##################

files=~/toedit/*

for f in $files
 do 

 mv /home/zope /gaspard/data
 sed '/[#Disable debug]/ a\[debug-mode on]' files

 done 


